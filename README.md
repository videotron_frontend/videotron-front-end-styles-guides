#Front end Style Guide

- [Javascript style guide](https://bitbucket.org/videotron-front-end/videotron-front-end-styles-guides/src/master/javascript.md?at=master)
- [Naming conventions](https://bitbucket.org/videotron-front-end/videotron-front-end-styles-guides/src/master/naming-conventions.md?at=master)
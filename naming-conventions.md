# naming conventions / rules

## General rules

- if the name is **meaningless**, it's not a good name :)
- don't mix english and french, well try to use english names only :)
- *general to detailed*:
    - `earth-america-canada-quebec-montreal`
    - `angular-app-v-business`

## Project name

A project name should follow this pattern: `category-name-identifier-name`

The `category-name` allow us to group related projects.
The `identifier-name` always start with `v`.

examples:

- `angular-app-v-business`
    - you should understand that :
        - the `category` is `angular-app` and the `identifier` is `v-business`
        - this is an AngularJS application
        - the module name of this application is `v-business`
	    - the npm package is `angular-app-v-business` (is it really necessary ? why not using the module name only ?)
- `angular-module-v-validation`
    - you should understand that :
        - the `category` is `angular-module` and the `identifier` is `v-validation`
    	- this is an AngularJS module (read component/library/plugin/... see: http://ngmodules.org/)
        - the module name of this module is `v-validation`
- `tools-v-yeoman-angular-generator`
    - you should understand that :
        - the `category` is `tools` and the `identifier` is `yeoman-angular-generator`
        - this is a Videotron AngularJS generator
- `tools-v-rest-api-stub`
    - you should understand that :
        - the `category` is `tools` and the `identifier` is `rest-api-stub`
        - this is a the rest api stub

## npm module

A npm module name should match the project `identifier`.

## Project root folder name

The project root folder name should match the project name.

## AngularJS modules

- module name should match the project `identifier`.

examples:

- `v-validation`
- `v-ui-carousel-owl`
- `v-ui-carousel-slick`
- `v-business-inputs`

## AngularJS files names

### Modules, config and run

An AngularJS module definition should be in a file named `module.js`.

A `config` block should be in a file named `config.js`

A `run` block should be in a file named `run.js`

### Controllers

An AngularJS controller name should end with `Controller`.

examples:

- `LoginController`
- `HomeController`

The file name should match the controller name.

examples:

- `LoginController.js`
- `HomeController.js`

### Services Factory Provider

Service, factory and provider names don't follow any specific rule.

examples:

- `Security`
- `SecurityService`
- `ModalHelper`
- `ModalService`
- `ItemFactory`

The file name should match the service name.

examples:

- `Security.js`
- `SecurityService.js`
- `ModalHelper.js`
- `ModalService.js`
- `ItemFactory.js`

### Directives

Directives names should start with a lowercase `v` and should be written using camelCase.

examples:

- `vInputValidator`
- `vOwlCarousel`
- `vRedButton`

The file name should match the directive name.

examples:

- `vInputValidator.js`
- `vOwlCarousel.js`
- `vRedButton.js`

### Tests

File name must match the tested file name and end with `.spec.js`

- `vInputValidator.spec.js`
- `FooService.spec.js`
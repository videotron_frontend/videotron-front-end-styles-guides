**v0.2.0** - 2015-11-30

- add naming conventions
- remove eslintrc (see: eslint-config-videotron-angularjs & eslint-config-videotron-javascript)
- remove tools directory (useless)	

**v0.1.0** - 2015-06-19

- release Javascript Style Guide and .eslintrc configuration file

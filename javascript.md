# Videotron Javascript style guide

## Table of Contents

1. [Foreword](#markdown-header-foreword)
1. [Linting](#markdown-header-linting)
1. [Types](#markdown-header-types)
1. [Objects](#markdown-header-objects)
1. [Arrays](#markdown-header-arrays)
1. [Strings](#markdown-header-strings)
1. [True and False Boolean Expressions](#markdown-header-true-and-false-boolean-expressions)
1. [Conditional (Ternary) Operator (?:)](#markdown-header-conditional-ternary-operator)
1. [&& and ||](#markdown-header-and)
1. [Functions](#markdown-header-functions)
1. [Explicit scope](#markdown-header-explicit-scope)
1. [Properties](#markdown-header-properties)
1. [Variables](#markdown-header-variables)
1. [Constants](#markdown-header-constants)
1. [Parentheses](#markdown-header-parentheses)
1. [Visibility (private and protected fields)](#markdown-header-visibility-private-and-protected-fields)
1. [Hoisting](#markdown-header-hoisting)
1. [Comparison Operators & Equality](#markdown-header-comparison-operators-equality)
1. [for-in loop](#markdown-header-for-in-loop)
1. [Iterating over Node Lists](#markdown-header-iterating-over-node-lists)
1. [Code blocks](#markdown-header-code-blocks)
1. [Comments](#markdown-header-comments)
1. [Whitespace](#markdown-header-whitespace)
1. [Commas](#markdown-header-commas)
1. [Semicolons](#markdown-header-semicolons)
1. [Naming Conventions](#markdown-header-naming-conventions)
1. [Getters and setters](#markdown-header-getters-and-setters)
1. [Classes](#markdown-header-constructors)
1. [Namespaces](#markdown-header-namespaces)
1. [delete](#markdown-header-delete)
1. [eval()](#markdown-header-eval)
1. [with(){}](#markdown-header-with)
1. [console.log()](#markdown-header-consolelog)
1. [Events](#markdown-header-events)
1. [Exceptions](#markdown-header-exceptions)
1. [Code formatting](#markdown-header-code-formatting)
1. [jQuery](#markdown-header-jquery)
1. [Standards features](#markdown-header-standards-features)
1. [ECMAScript 5 Compatibility](#markdown-header-ecmascript-5-compatibility)
1. [Internet Explorer's Conditional Comments](#markdown-header-internet-explorers-conditional-comments)
1. [Testing](#markdown-header-testing)
1. [Examples](#markdown-header-examples)
1. [Credits](#markdown-header-credits)

## Foreword

**BE CONSISTENT!** If you're editing code, take a few minutes to look at the code around you and determine its style. If they use spaces around all their arithmetic operators, you should too. If their comments have little boxes of hash marks around them, make your comments have little boxes of hash marks around them too.

The point of having style guidelines is to have a common vocabulary of coding so people can concentrate on what you're saying rather than on how you're saying it. We present global style rules here so people know the vocabulary, but local style is also important. If code you add to a file looks drastically different from the existing code around it, it throws readers out of their rhythm when they go to read it. Avoid this.

**[back to top](#markdown-header-table-of-contents)**

***

## Linting

You should :

- use [eslint](https://github.com/eslint/eslint) linter.
- use `.eslintrc` file provided in this repository.

**[back to top](#markdown-header-table-of-contents)**

***

## Types

### Primitives

**[eslint: n/a](http://eslint.org/)**

When you access a primitive type you work directly on its value.

+ `string`
+ `number`
+ `boolean`
+ `null`
+ `undefined`

```javascript
var foo = 1;
var bar = foo;

// changing the value of bar will not change the value of foo
bar = 9;

console.log(foo, bar); // 1 9
```

### Wrapper objects for primitive types

**[eslint: no-new-wrappers](http://eslint.org/docs/rules/no-new-wrappers.html)**

**Evil!** There's no reason to use wrapper objects for primitive types, plus they're dangerous.

```javascript
// bad
var x = new Boolean(false);
if (x) {
    alert('hi');  // Shows 'hi'.
}
```

Don't do it! However type casting is fine.

```javascript
var x = Boolean(0);
if (x) {
    alert('hi');  // This will never be alerted.
}
typeof Boolean(0) == 'boolean';
typeof new Boolean(0) == 'object';
```

### Complex

**[eslint: n/a](http://eslint.org/)**

When you access a complex type you work on a **reference** to its value.

+ `object`
+ `array`
+ `function`

```javascript
var foo = [1, 2];
var bar = foo;

bar[0] = 9;

console.log(foo[0], bar[0]); // 1 9
```

**[back to top](#markdown-header-table-of-contents)**

***

## Objects

### Creation

**[eslint: no-new-object](http://eslint.org/docs/rules/no-new-object.html)**

Use the literal syntax for object creation.

```javascript
// bad
var item = new Object();

// good
var item = {};
```

### Reserved words

**[eslint: no-reserved-keys](http://eslint.org/docs/rules/no-reserved-keys.html)**

Don't use [reserved words](http://es5.github.io/#x7.6.1) as keys. It won't work in IE8. [More info](https://github.com/airbnb/javascript/issues/61).

```javascript
// bad
var superman = {
    default: { clark: 'kent' },
    private: true
};

// good
var superman = {
    defaults: { clark: 'kent' },
    hidden: true
};
```

Use readable synonyms in place of reserved words.

```javascript
// bad
var superman = {
    class: 'alien'
};

// bad
var superman = {
    klass: 'alien'
};

// good
var superman = {
    type: 'alien'
};
```

### Method and property definitions

**[eslint: n/a](http://eslint.org/)**

While there are several ways to attach methods and properties to an object created via `new`, the preferred style for methods is:

```javascript
Foo.prototype.bar = function() {
    /* ... */
};
```

The preferred style for other properties is to initialize the field in the constructor:

```javascript
/** @constructor */
function Foo() {
    this.bar = value;
}
```

Current JavaScript engines optimize based on the "shape" of an object, [adding a property to an object (including overriding a value set on the prototype) changes the shape and can degrade performance](https://developers.google.com/v8/design#prop_access).

**[back to top](#markdown-header-table-of-contents)**

***

## Arrays

### Creation

**[eslint: no-array-constructor](http://eslint.org/docs/rules/no-array-constructor.html)**

Use the literal syntax for array creation.

```javascript
// bad
var items = new Array();

// good
var items = [];
```

### Assignment

**[eslint: n/a](http://eslint.org/)**

Use `Array.push` instead of direct assignment to add items to an array.

```javascript
var someStack = [];

// bad
someStack[someStack.length] = 'abracadabra';

// good
someStack.push('abracadabra');
```

### Cloning

**[eslint: n/a](http://eslint.org/)**

When you need to copy an array use `Array.slice()` (see: [jsPerf](http://jsperf.com/converting-arguments-to-an-array/7)).

```javascript
var len = items.length;
var itemsCopy = [];
var i;

// bad
for (i = 0; i < len; i++) {
    itemsCopy[i] = items[i];
}

// good
itemsCopy = items.slice();
```

### Convert

**[eslint: n/a](http://eslint.org/)**

To convert an array-like object to an array, use `Array.slice()`.

```javascript
function trigger() {
    var args = Array.prototype.slice.call(arguments);
    //...
}
```

### Non number indexes

**[eslint: n/a](http://eslint.org/)**

Never use `Array` as a map/hash/associative array. Associative `Arrays` are not allowed... or more precisely you are not allowed to use non number indexes for arrays. If you need a map/hash use `Object` instead of `Array` in these cases because the features that you want are actually features of `Object` and not of Array. Array just happens to extend `Object` (like any other object in JS and therefore you might as well have used `Date`, `RegExp` or `String`).

```javascript
var someStack = [];

// bad
someStack['item'] = 'stuff';

var key = 'magic-key';

// bad
someStack[key] = 'stuff';
```

**[back to top](#markdown-header-table-of-contents)**

***

## Strings

### Single quotes

**[eslint: quotes](http://eslint.org/docs/rules/quotes.html)**

Use single quotes `''` for strings.

```javascript
// bad
var name = "Bob Parr";

// bad
var fullName = "Bob " + this.lastName;

// good
var name = 'Bob Parr';

// good
var fullName = 'Bob ' + this.lastName;
```

### Long strings

**[eslint: n/a](http://eslint.org/)**

Strings longer than 80 characters should be written across multiple lines using string concatenation.

Note: If overused, long strings with concatenation could impact performance (see: [jsPerf](http://jsperf.com/ya-string-concat) & [Discussion](https://github.com/airbnb/javascript/issues/40)).

```javascript
// bad
var errorMessage = 'This is a super long error that was thrown because of Batman. When you stop to think about how Batman had anything to do with this, you would get nowhere fast.';

// bad
var errorMessage = 'This is a super long error that was thrown because \
of Batman. When you stop to think about how Batman had anything to do \
with this, you would get nowhere \
fast.';

// good
var errorMessage = 'This is a super long error that was thrown because ' +
  'of Batman. When you stop to think about how Batman had anything to do ' +
  'with this, you would get nowhere fast.';
```

### Concatenation

**[eslint: n/a](http://eslint.org/)**

When programmatically building up a string, use `Array.join` instead of string concatenation (mostly for IE, see: [jsPerf](http://jsperf.com/string-vs-array-concat/2)).

```javascript
var items;
var messages;
var length;
var i;

messages = [{
    state: 'success',
    message: 'This one worked.'
  }, {
    state: 'success',
    message: 'This one worked as well.'
  }, {
    state: 'error',
    message: 'This one did not work.'
}];

length = messages.length;

// bad
function inbox(messages) {
    items = '<ul>';

    for (i = 0; i < length; i++) {
        items += '<li>' + messages[i].message + '</li>';
    }

  return items + '</ul>';
}

// good
function inbox(messages) {
  items = [];

    for (i = 0; i < length; i++) {
        // use direct assignment in this case because we're micro-optimizing.
        items[i] = '<li>' + messages[i].message + '</li>';
    }

    return '<ul>' + items.join('') + '</ul>';
}
```

**[back to top](#markdown-header-table-of-contents)**

***

## True and False Boolean Expressions

### Good to know

The following are all false in boolean expressions:

- `null`
- `undefined`
- `''` empty string
- `0` the number

But be careful, because these are all true:

- `'0'` the string
- `[]` empty array
- `{}` empty object

### null checking

**[eslint: n/a](http://eslint.org/)**

Instead of this:

```javascript
// bad
while (x != null) {}
```

you can write this shorter code (as long as you don't expect x to be 0, or the empty string, or false):

```javascript
// good
while (x) {}
```

And if you want to check a string to see if it is null or empty, you could do this:

```javascript
// bad
if (y != null && y != '') {}
```

But this is shorter and nicer:

```javascript
// good
if (y) {}
```

### Reminder

There are many unintuitive things about boolean expressions. Here are some of them:

```javascript
Boolean('0') == true
'0' != true
0 != null
0 == []
0 == false
Boolean(null) == false
null != true
null != false
Boolean(undefined) == false
undefined != true
undefined != false
Boolean([]) == true
[] != true
[] == false
Boolean({}) == true
{} != true
{} != false
```

**[back to top](#markdown-header-table-of-contents)**

***

## Conditional (Ternary) Operator (?:)

### Can be useful

Instead of this:

```javascript
if (val) {
    return foo();
} else {
    return bar();
}
```

you can write this:

```javascript
return val ? foo() : bar();
```

The ternary conditional is also useful when generating HTML:

```javascript
var html = '<input type="checkbox"' +
    (isChecked ? ' checked' : '') +
    (isEnabled ? '' : ' disabled') +
    ' name="foo">';
```

### Nested ternary operators

**[eslint: no-nested-ternary](http://eslint.org/docs/rules/no-nested-ternary.html)**

You should not nest ternary operators.

```javascript
// bad
var value = 1 > 2 ? 2 < 3 ? : 'a' : 'b';
```

### Unneeded ternary operator

**[eslint: no-unneeded-ternary](http://eslint.org/docs/rules/no-unneeded-ternary.html)**

You should not use a ternary operators when it's not needed.

```javascript
// bad
var booleanResult = 1 > 2 ? true : false;

// good
var booleanResult = 1 > 2;
```

**[back to top](#markdown-header-table-of-contents)**

***

## && and ||

### Good to know

These binary boolean operators are short-circuited, and evaluate to the last evaluated term.

`||` has been called the 'default' operator, because instead of writing this:

```javascript
function foo(optWin) {
    var win;
    if (optWin) {
        win = optWin;
    } else {
        win = window;
    }
}
```

you can write this:

```javascript
function foo(optWin) {
    var win = optWin || window;
}
```

`&&` is also useful for shortening code. For instance, instead of this:

```javascript
// bad
if (node) {
    if (node.kids) {
        if (node.kids[index]) {
          foo(node.kids[index]);
        }
    }
}
```

you could do this:

```javascript
// nice :)
if (node && node.kids && node.kids[index]) {
    foo(node.kids[index]);
}
```

or this:

```javascript
// nice :)
var kid = node && node.kids && node.kids[index];
if (kid) {
    foo(kid);
}
```

However, this is going a little too far:

```javascript
// evil :(
node && node.kids && node.kids[index] && foo(node.kids[index]);
```

**[back to top](#markdown-header-table-of-contents)**

***

## Functions

### Style

**[eslint: func-style](http://eslint.org/docs/rules/func-style.html)**

You should use function declaration. The primary difference between function declarations and function expressions is that declarations are hoisted to the top of the scope in which they are defined, which allows you to write code that uses the function before the declaration. It also mean that you don't have to worries about knowing if the function is available or not :)

```javascript
// good
function foo(){

}
```

You should not use function expressions.

```javascript
// not bad but we favor function declaration
var anonymous = function() {
    return true;
};

// not bad but we favor function declaration
var named = function named() {
    return true;
};

// note that you can do that
function foo(){}
function bar(){}

var action;

if(someTestResult){
    action = foo;
}else{
    action = bar;
}
```

### Non-function blocks

**[eslint: no-inner-declarations](http://eslint.org/docs/rules/no-inner-declarations.html)**

Never declare a function in a non-function block (`if`, `while`, etc). Assign the function to a variable instead. Browsers will allow you to do it, but they all interpret it differently, which is bad news bears.

_Note: ECMA-262 defines a `block` as a list of statements. A function declaration is not a statement. [Read ECMA-262's note on this issue](http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf#page=97)._

```javascript
// bad
if (currentUser) {
    function test() {
      console.log('Nope.');
    }
}

// good
var test;
if (currentUser) {
    test = function foo() {
      console.log('Style Is The Message.');
    };
}
```

### arguments

**[eslint: no-shadow-restricted-names](http://eslint.org/docs/rules/no-shadow-restricted-names.html)**

Never name a parameter `arguments`. This will take precedence over the `arguments` object that is given to every function scope.

```javascript
// bad
function nope(name, options, arguments) {
    // ...stuff...
}

// good
function yup(name, options, args) {
    // ...stuff...
}
```

**[back to top](#markdown-header-table-of-contents)**

***

### Closures

**[eslint: n/a](http://eslint.org/)**

Yes, but be careful. The ability to create closures is perhaps the most useful and often overlooked feature of JS. [Here is a good description of how closures work](http://jibbering.com/faq/faq_notes/closures.html). One thing to keep in mind, however, is that a closure keeps a pointer to its enclosing scope. As a result, attaching a closure to a DOM element can create a circular reference and thus, a memory leak.

```javascript
//closure example from : http://jibbering.com/faq/faq_notes/closures.html
function callLater(paramA, paramB, paramC){
    /* Return a reference to an anonymous inner function created
       with a function expression:-
    */
    return (function(){
        /* This inner function is to be executed with - setTimeout
           - and when it is executed it can read, and act upon, the
           parameters passed to the outer function:-
        */
        paramA[paramB] = paramC;
    });
}

...

/* Call the function that will return a reference to the inner function
   object created in its execution context. Passing the parameters that
   the inner function will use when it is eventually executed as
   arguments to the outer function. The returned reference to the inner
   function object is assigned to a local variable:-
*/
var functRef = callLater(elStyle, "display", "none");
/* Call the setTimeout function, passing the reference to the inner
   function assigned to the - functRef - variable as the first argument:-
*/
hideMenu=setTimeout(functRef, 500);
```

### Nested function

**[eslint: n/a](http://eslint.org/)**

Yes! Nested functions can be very useful, for example in the creation of [continuations](http://matt.might.net/articles/by-example-continuation-passing-style/) and for the task of hiding helper functions. Feel free to use them.

```javascript
function transform(data){
    // this is a nested function
    function applyTrans(){
    }

    var tempData = computingHelper()
}

doStuff();
```

**[back to top](#markdown-header-table-of-contents)**

***

**[back to top](#markdown-header-table-of-contents)**

***

## Explicit scope

**[eslint: n/a](http://eslint.org/)**

Try to always use an explicit scope. Doing so increases portability and clarity. For example, don't rely on `window` being in the scope chain. You might want to use your function in another application for which window is not the content window.

```javascript
// bad
localStorage.setItem('lucky-key',777);

// good
window.localStorage.setItem('lucky-key',777);


// good
(function(window){

    window.localStorage.setItem('lucky-key',777);

})(window);
```

**[back to top](#markdown-header-table-of-contents)**

***

## Properties

**[eslint: dot-notation](http://eslint.org/docs/rules/dot-notation.html)**

Use dot notation when accessing properties.

```javascript
var luke = {
    jedi: true,
    age: 28
};

// bad
var isJedi = luke['jedi'];

// good
var isJedi = luke.jedi;
```

Use subscript notation `[]` when accessing properties with a variable.

```javascript
var luke = {
    jedi: true,
    age: 28
};

function getProp(prop) {
    return luke[prop];
}

var isJedi = getProp('jedi');
```

**[back to top](#markdown-header-table-of-contents)**

***

## Variables

### Always use var keyword

**[eslint: block-scoped-var](http://eslint.org/docs/rules/block-scoped-var.html)**

Always use `var` to declare variables. Not doing so will result in global variables. We want to avoid polluting the global namespace. Captain Planet warned us of that.

```javascript
// bad
superPower = new SuperPower();

// good
var superPower = new SuperPower();
```

### One var per variable

**[eslint: one-var](http://eslint.org/docs/rules/one-var.html)**

Use **one** `var` declaration per variable.

It's easier to add new variable declarations this way, and you never have to worry about swapping out a `;` for a `,` or introducing punctuation-only diffs.

```javascript
// bad
var items = getItems(),
    goSportsTeam = true,
    dragonball = 'z';

// bad
// (compare to above, and try to spot the mistake)
var items = getItems(),
    goSportsTeam = true;
    dragonball = 'z';

// good
var items = getItems();
var goSportsTeam = true;
var dragonball = 'z';
```

### Unassigned variables last

**[eslint: n/a](http://eslint.org/)**

Declare unassigned variables last. This is helpful when later on you might need to assign a variable depending on one of the previous assigned variables.

```javascript
// bad
var i, len, dragonball,
    items = getItems(),
    goSportsTeam = true;

// bad
var i;
var items = getItems();
var dragonball;
var goSportsTeam = true;
var len;

// good
var items = getItems();
var goSportsTeam = true;
var dragonball;
var length;
var i;
```

### Var on top

**[eslint: vars-on-top](http://eslint.org/docs/rules/vars-on-top.html)**

Assign variables at the top of their scope. This helps avoid issues with variable declaration and assignment hoisting related issues.

```javascript
// bad
function() {
  test();
  console.log('doing stuff..');

  //..other stuff..

  var name = getName();

    if (name === 'test') {
        return false;
    }

  return name;
}

// good
function() {
    var name = getName();

    test();
    console.log('doing stuff..');

    //..other stuff..

    if (name === 'test') {
        return false;
    }

    return name;
}

// bad - unnessary function call
function() {
    var name = getName();

    if (!arguments.length) {
        return false;
    }

    this.setFirstName(name);

    return true;
}

// good
function() {
    var name;

    if (!arguments.length) {
        return false;
    }

    name = getName();
    this.setFirstName(name);

    return true;
}
```

### Deferred initialization

**[eslint: n/a](http://eslint.org/)**

It isn't always possible to initialize variables at the point of declaration, so deferred initialization is fine.

**[back to top](#markdown-header-table-of-contents)**

***

## Constants

**[eslint: n/a](http://eslint.org/)**

If a value is intended to be constant and immutable : use `NAMES_LIKE_THIS` for constant values. Example:

```javascript
//bad
var someElementPosition = 15 - element.height();

//good
var GLOBAL_MARGIN_TOP = 15;
var someElementPosition = GLOBAL_MARGIN_TOP - element.height();
```

**[back to top](#markdown-header-table-of-contents)**

***

## Parentheses

**[eslint: no-extra-parens](http://eslint.org/docs/rules/no-extra-parens.html)**

**Only where required**. Use sparingly and in general only where required by the syntax and semantics. Never use parentheses for unary operators such as delete, typeof and void or after keywords such as return, throw as well as others (case, in or new).

```javascript
// bad
var result = (1 + someRandomNumber);

// good
var result = 1 + someRandomNumber;

// good
var result = (1 + someRandomNumber) / someOtherRandomNumber;

// evil!
var dragonColor = ((dragon).getColor).call(null);
```

**[back to top](#markdown-header-table-of-contents)**

***

## Visibility (private and protected fields)

### JSDoc

**[eslint: valid-jsdoc](http://eslint.org/docs/rules/valid-jsdoc.html)**

Encouraged, use JSDoc annotations `@private` and `@protected`.

We recommend the use of the JSDoc annotations `@private` and `@protected` to indicate visibility levels for classes, functions, and properties.

### Underscore

**[eslint: no-underscore-dangle](http://eslint.org/docs/rules/no-underscore-dangle.html)**

You should not use an underscore for private members.

```javascript
// bad
var _code = '123456';

// good
var code = '123456';
```

### Who to create private and public members

Here is a quick example :

```javascript

// age is a private member
function Foo(age){
    // this function is private
    function formatAge(value){
        return value;
    }

    // this budy is private
    var classRoomId;

    // this guy is public
    this.getAge = function(){
        return age;
    };

    // this guy is public
    this.setAge = function(value){
        age = formatAge(value);
    };

}

var foo = new Foo(1);

// 1
foo.getAge();

foo.setAge(2);
// 1
foo.getAge();

// undefined
foo.age

```

**[back to top](#markdown-header-table-of-contents)**

***

## Hoisting

_For more information refer to [JavaScript Scoping & Hoisting](http://www.adequatelygood.com/2010/2/JavaScript-Scoping-and-Hoisting) by [Ben Cherry](http://www.adequatelygood.com/)._

### Variable declarations

**[eslint: n/a](http://eslint.org/)**

Variable declarations get hoisted to the top of their scope, but their assignment does not.

```javascript
// we know this wouldn't work (assuming there
// is no notDefined global variable)
function example() {
  console.log(notDefined); // => throws a ReferenceError
}

// creating a variable declaration after you
// reference the variable will work due to
// variable hoisting. Note: the assignment
// value of `true` is not hoisted.
function example() {
    console.log(declaredButNotAssigned); // => undefined
    var declaredButNotAssigned = true;
}

// The interpreter is hoisting the variable
// declaration to the top of the scope,
// which means our example could be rewritten as:
function example() {
    var declaredButNotAssigned;
    console.log(declaredButNotAssigned); // => undefined
    declaredButNotAssigned = true;
}
```

### Anonymous functions expressions

**[eslint: n/a](http://eslint.org/)**

Anonymous function expressions hoist their variable name, but not the function assignment.

```javascript
function example() {
    console.log(anonymous); // => undefined

    anonymous(); // => TypeError anonymous is not a function

    var anonymous = function() {
        console.log('anonymous function expression');
  };
}
```

### Named function expressions

**[eslint: n/a](http://eslint.org/)**

Named function expressions hoist the variable name, not the function name or the function body.

```javascript
function example() {
    console.log(named); // => undefined

    named(); // => TypeError named is not a function

    superPower(); // => ReferenceError superPower is not defined

    var named = function superPower() {
      console.log('Flying');
    };
}

// the same is true when the function name
// is the same as the variable name.
function example() {
    console.log(named); // => undefined

    named(); // => TypeError named is not a function

    var named = function named() {
        console.log('named');
    }
}
```

### Function declarations

**[eslint: n/a](http://eslint.org/)**

Function declarations hoist their name and the function body.

```javascript
function example() {
    superPower(); // => Flying

    function superPower() {
        console.log('Flying');
    }
}
```

**[back to top](#markdown-header-table-of-contents)**

***

## Comparison Operators & Equality

**[eslint: eqeqeq](http://eslint.org/docs/rules/eqeqeq.html)**

Use `===` and `!==` over `==` and `!=`.

Conditional statements such as the `if` statement evaulate their expression using coercion with the `ToBoolean` abstract method and always follow these simple rules:

+ **Objects** evaluate to **true**
+ **Undefined** evaluates to **false**
+ **Null** evaluates to **false**
+ **Booleans** evaluate to **the value of the boolean**
+ **Numbers** evaluate to **false** if **+0, -0, or NaN**, otherwise **true**
+ **Strings** evaluate to **false** if an empty string `''`, otherwise **true**

```javascript
var a = [];
if (a) {
    // true
    // An array is an object, objects evaluate to true
}
```

Use shortcuts.

```javascript
// bad
if (name !== '') {
    // ...stuff...
}

// good
if (name) {
    // ...stuff...
}

// bad
if (collection.length > 0) {
    // ...stuff...
}

// good
if (collection.length) {
    // ...stuff...
}
```

For more information see [Truth Equality and JavaScript](http://javascriptweblog.wordpress.com/2011/02/07/truth-equality-and-javascript/#more-2108) by Angus Croll.

**[back to top](#markdown-header-table-of-contents)**

***

## for-in loop

**[eslint: n/a](http://eslint.org/)**

Only for iterating over keys in an object/map/hash. `for-in loops` are often incorrectly used to loop over the elements in an `Array`. This is however very error prone because it does not loop from 0 to length - 1 but over all the present keys in the object and its prototype chain.

Always use normal for loops when using arrays.

```javascript
var arr = [1,2,3];

// bad
for (var k in arr) {
    console.log(arr[i]);
}


// good
for (var i = 0,count = arr.length; i < count; i++) {
    console.log(arr[i]);
}
```

**[back to top](#markdown-header-table-of-contents)**

***

## Iterating over Node Lists

**[eslint: n/a](http://eslint.org/)**

Node lists are often implemented as node iterators with a filter. This means that getting a property like length is O(n), and iterating over the list by re-checking the length will be O(n^2).

```javascript
var paragraphs = document.getElementsByTagName('p');
for (var i = 0; i < paragraphs.length; i++) {
    doSomething(paragraphs[i]);
}
```

It is better to do this instead:

```javascript
var paragraphs = document.getElementsByTagName('p');
for (var i = 0, paragraph; paragraph = paragraphs[i]; i++) {
    doSomething(paragraph);
}
```

This works well for all collections and arrays as long as the array does not contain things that are treated as boolean false.

In cases where you are iterating over the childNodes you can also use the firstChild and nextSibling properties.

```javascript
var parentNode = document.getElementById('foo');
for (var child = parentNode.firstChild; child; child = child.nextSibling) {
    doSomething(child);
}
```

**[back to top](#markdown-header-table-of-contents)**

***

## Code blocks

**[eslint: curly](http://eslint.org/docs/rules/curly.html)**

Use braces with all multi-line blocks.

```javascript
// bad
if (test)
    return false;

// bad
if (test) return false;

// good
if (test) {
    return false;
}

// good
function() {
    return false;
}
```

If you're using multi-line blocks with `if` and `else`, put `else` on the same line as your

`if` block's closing brace.

```javascript
// bad
if (test) {
    thing1();
    thing2();
}
else {
    thing3();
}

// good
if (test) {
    thing1();
    thing2();
} else {
    thing3();
}
```

**[back to top](#markdown-header-table-of-contents)**

***

## Comments

You should let Your Code Speak For Itself.

Unless your are coding a **real reusable component, you should not add JSDoc to your code**.

### Multi lines comments

**[eslint: n/a](http://eslint.org/)**

Use `/** ... */` for multi-line comments. Include a description, specify types and values for all parameters and return values.

```javascript
// bad
// make() returns a new element
// based on the passed in tag name
//
// @param {String} tag
// @return {Element} element
function make(tag) {

    // ...stuff...

    return element;
}

// good
/**
 * make() returns a new element
 * based on the passed in tag name
 *
 * @param {String} tag
 * @return {Element} element
 */
function make(tag) {

    // ...stuff...

    return element;
}
```

### Single lines comments

**[eslint: no-inline-comments](http://eslint.org/docs/rules/no-inline-comments.html)**

Use `//` for single line comments. Place single line comments on a newline above the subject of the comment. Put an empty line before the comment.

```javascript
// bad
var active = true;  // is current tab

// good
// is current tab
var active = true;

// bad
function getType() {
    console.log('fetching type...');
    // set the default type to 'no type'
    var type = this._type || 'no type';

    return type;
}

// good
function getType() {
    console.log('fetching type...');

    // set the default type to 'no type'
    var type = this._type || 'no type';

    return type;
}
```

Prefixing your comments with `TODO` helps other developers quickly understand if you're pointing out a problem that needs to be revisited, or if you're suggesting a solution to the problem that needs to be implemented. These are different than regular comments because they are actionable. The actions are `FIXME -- need to figure this out` or `TODO -- need to implement`.

### FIXEME

**[eslint: no-warning-comments](http://eslint.org/docs/rules/no-warning-comments.html)**

You should not use `// FIXME:` to annotate problems. Fix your problem then commit your code : )

```javascript
function Calculator() {
    // bad
    // FIXME: shouldn't use a global here
    total = 0;

    return this;
}
```

### TODO

**[eslint: no-warning-comments](http://eslint.org/docs/rules/no-warning-comments.html)**

Use `// TODO:` to annotate solutions to problems.

```javascript
function Calculator() {

    // TODO: total should be configurable by an options param
    this.total = 0;

    return this;
}
```

### JSDoc

**[eslint: valid-jsdoc](http://eslint.org/docs/rules/valid-jsdoc.html)**

Use JSDoc. The JSDoc syntax is based on JavaDoc. Many tools extract metadata from JSDoc comments to perform code validation and optimizations. These comments must be well-formed.

```
#!javascript
/**
 * A JSDoc comment should begin with a slash and 2 asterisks.
 * Inline tags should be enclosed in braces like {@code this}.
 * @desc Block tags should always start on their own line.
 */
```

### Top/File-Level Comments

**[eslint: n/a](http://eslint.org/)**

A copyright notice and author information are **optional**. File overviews are generally recommended whenever a file consists of more than a single class definition. The top level comment is designed to orient readers unfamiliar with the code to what is in this file. If present, it should provide a description of the file's contents and any dependencies or compatibility information. As an example:

```
#!javascript
/**
 * @fileoverview Description of file, its uses and information
 * about its dependencies.
 */
```

**[back to top](#markdown-header-table-of-contents)**

***

## Whitespace

### Indentation

**[eslint: indent](http://eslint.org/docs/rules/indent.html)**

Use soft tabs set to 4 spaces.

```javascript
// good
function() {
∙∙∙∙var name;
}

// bad
function() {
∙var name;
}

// bad
function() {
∙∙var name;
}
```

### Leading brace

**[eslint: space-before-blocks](http://eslint.org/docs/rules/space-before-blocks.html)**

Place 1 space before the leading brace.

```javascript
// bad
function test(){
    console.log('test');
}

// good
function test() {
    console.log('test');
}

// bad
dog.set('attr',{
    age: '1 year',
    breed: 'Bernese Mountain Dog'
});

// good
dog.set('attr', {
    age: '1 year',
    breed: 'Bernese Mountain Dog'
});
```

### Opening parenthesis

**[eslint: space-after-keywords](http://eslint.org/docs/rules/space-after-keywords.html)**

Place 1 space before the opening parenthesis in control statements (`if`, `while` etc.). Place no space before the argument list in function calls and declarations.

```javascript
// bad
if(isJedi) {
    fight ();
}

// good
if (isJedi) {
    fight();
}

// bad
function fight () {
    console.log ('Swooosh!');
}

// good
function fight() {
    console.log('Swooosh!');
}
```

Set off operators with spaces.

```javascript
// bad
var x=y+5;

// good
var x = y + 5;
```

### EOF

**[eslint: n/a](http://eslint.org/)**

End files with a single newline character.

```javascript
// bad
(function(global) {
    // ...stuff...
})(this);
```

```javascript
// bad
(function(global) {
    // ...stuff...
})(this);↵
↵
```

```javascript
// good
(function(global) {
    // ...stuff...
})(this);↵
```

### Methods chaining

**[eslint: n/a](http://eslint.org/)**

Use indentation when making long method chains. Use a leading dot, which emphasizes that the line is a method call, not a new statement.

```javascript
// bad
$('#items').find('.selected').highlight().end().find('.open').updateCount();

// bad
$('#items').
    find('.selected').
        highlight().
        end().
    find('.open').
       updateCount();

// good
$('#items')
    .find('.selected')
        .highlight()
        .end()
    .find('.open')
        .updateCount();

// bad
var leds = stage.selectAll('.led').data(data).enter().append('svg:svg').classed('led', true)
    .attr('width', (radius + margin) * 2).append('svg:g')
    .attr('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')')
    .call(tron.led);

// good
var leds = stage.selectAll('.led')
        .data(data)
    .enter().append('svg:svg')
        .classed('led', true)
        .attr('width', (radius + margin) * 2)
    .append('svg:g')
        .attr('transform', 'translate(' + (radius + margin) + ',' + (radius + margin) + ')')
        .call(tron.led);
```

### Blank line after blocks and before the next statement

**[eslint: n/a](http://eslint.org/)**

Leave a blank line after blocks and before the next statement

```javascript
// bad
if (foo) {
    return bar;
}
return baz;

// good
if (foo) {
    return bar;
}

return baz;

// bad
var obj = {
    foo: function() {
    },
    bar: function() {
    }
};
return obj;

// good
var obj = {
    foo: function() {
    },

    bar: function() {
    }
};

return obj;
```

**[back to top](#markdown-header-table-of-contents)**

***

## Commas

**[eslint: comma-spacing](http://eslint.org/docs/rules/comma-spacing.html)**
**[eslint: comma-style](http://eslint.org/docs/rules/comma-style.html)**

Leading commas: **Nope.**

```javascript
// bad
var story = [
      once
    , upon
    , aTime
];

// good
var story = [
    once,
    upon,
    aTime
];

// bad
var hero = {
     firstName: 'Bob'
   , lastName: 'Parr'
   , heroName: 'Mr. Incredible'
   , superPower: 'strength'
};

// good
var hero = {
    firstName: 'Bob',
    lastName: 'Parr',
    heroName: 'Mr. Incredible',
    superPower: 'strength'
};
```

Additional trailing comma: **Nope.** This can cause problems with IE6/7 and IE9 if it's in quirksmode. Also, in some implementations of ES3 would add length to an array if it had an additional trailing comma. This was clarified in ES5 ([source](http://es5.github.io/#D)):

> Edition 5 clarifies the fact that a trailing comma at the end of an ArrayInitialiser does not add to the length of the array. This is not a semantic change from Edition 3 but some implementations may have previously misinterpreted this.

```javascript
// bad
var hero = {
    firstName: 'Kevin',
    lastName: 'Flynn',
};

var heroes = [
    'Batman',
    'Superman',
];

// good
var hero = {
    firstName: 'Kevin',
    lastName: 'Flynn'
};

var heroes = [
    'Batman',
    'Superman'
];
```

**[back to top](#markdown-header-table-of-contents)**

***

## Semicolons

**[eslint: semi](http://eslint.org/docs/rules/semi.html)**

**Yup.**

```javascript
// bad
(function() {
    var name = 'Skywalker'
    return name
})()

// good
(function() {
    var name = 'Skywalker';
    return name;
})();

// good (guards against the function becoming an argument when two files with IIFEs are concatenated)
;(function() {
    var name = 'Skywalker';
    return name;
})();
```

[Read more](http://stackoverflow.com/a/7365214/1712802).

**[back to top](#markdown-header-table-of-contents)**

***

## Type Casting & Coercion

Perform type coercion at the beginning of the statement.

### Strings

**[eslint: no-new-wrappers](http://eslint.org/docs/rules/no-new-wrappers.html)**

You don't have to cast to `String` if you concatenate your value with a non empty `String`. If you need to cast your value to `String` use : `String(value)`.

```javascript
// bad
var totalScore = this.reviewScore + '';

// bad
var totalScore = '' + this.reviewScore;

// bad
var totalScore = '' + this.reviewScore + ' total score';

// good
var totalScore = this.reviewScore + ' total score';

// good
var totalScore = String(this.reviewScore);
```

### Numbers

**[eslint: no-new-wrappers](http://eslint.org/docs/rules/no-new-wrappers.html)**
**[eslint: radix](http://eslint.org/docs/rules/radix.html)**

Use `parseInt` for Numbers and always with a radix for type casting.

```javascript
var inputValue = '4';

// bad
var val = new Number(inputValue);

// bad
var val = +inputValue;

// bad
var val = inputValue >> 0;

// bad
var val = parseInt(inputValue);

// good
var val = Number(inputValue);

// good
var val = parseInt(inputValue, 10);
```

If for whatever reason you are doing something wild and `parseInt` is your bottleneck and need to use Bitshift for [performance reasons](http://jsperf.com/coercion-vs-casting/3), leave a comment explaining why and what you're doing.

```javascript
// good
/**
 * parseInt was the reason my code was slow.
 * Bitshifting the String to coerce it to a
 * Number made it a lot faster.
 */
var val = inputValue >> 0;
```

Note: Be careful when using bitshift operations. Numbers are represented as [64-bit values](http://es5.github.io/#x4.3.19), but Bitshift operations always return a 32-bit integer ([source](http://es5.github.io/#x11.7)). Bitshift can lead to unexpected behavior for integer values larger than 32 bits. [Discussion](https://github.com/airbnb/javascript/issues/109). Largest signed 32-bit Int is 2,147,483,647:

```javascript
2147483647 >> 0 //=> 2147483647
2147483648 >> 0 //=> -2147483648
2147483649 >> 0 //=> -2147483647
```

### Booleans

**[eslint: no-new-wrappers](http://eslint.org/docs/rules/no-new-wrappers.html)**

```javascript
var age = 0;

// bad
var hasAge = new Boolean(age);

// good
var hasAge = Boolean(age);

// good
var hasAge = !!age;
```

**[back to top](#markdown-header-table-of-contents)**

***

## Naming Conventions

In general, use `functionNamesLikeThis`, `variableNamesLikeThis`, `ClassNamesLikeThis`, `EnumNamesLikeThis`, `methodNamesLikeThis`, `CONSTANT_VALUES_LIKE_THIS`, `foo.namespaceNamesLikeThis.bar`, and `filenameslikethis.js`.

### Short variable name

**[eslint: n/a](http://eslint.org/)**

Avoid single letter names. Be descriptive with your naming.

```javascript
// bad
function q() {
    // ...stuff...
}

// good
function query() {
    // ..stuff..
}
```

### Camelcase

**[eslint: camelcase](http://eslint.org/docs/rules/camelcase.html)**

Use camelCase when naming objects, functions, and instances.

```javascript
// bad
var OBJEcttsssss = {};
var this_is_my_object = {};
var o = {};
function c() {}

// good
var thisIsMyObject = {};
function thisIsMyFunction() {}
```

Use PascalCase when naming constructors or classes.

```javascript
// bad
function user(options) {
    this.name = options.name;
}

var bad = new user({
  name: 'nope'
});

// good
function User(options) {
    this.name = options.name;
}

var good = new User({
    name: 'yup'
});
```

### Private variables

**[eslint: no-underscore-dangle](http://eslint.org/docs/rules/no-underscore-dangle.html)**

Don't use a leading underscore `_` when naming private properties.

```javascript
// bad
this.__firstName__ = 'Panda';
this.firstName_ = 'Panda';

// bad
this._firstName = 'Panda';
```

### References to this

**[eslint: consistent-this](http://eslint.org/docs/rules/consistent-this.html)**

When saving a reference to `this` use `self`.

```javascript
// bad
function() {
    var _this = this;
    return function() {
        console.log(_this);
  };
}

// bad
function() {
    var that = this;
    return function() {
        console.log(that);
  };
}

// good
function() {
    var self = this;
    return function() {
        console.log(self);
  };
}
```

### Anonymous functions

**[eslint: n/a](http://eslint.org/)**

Name your functions. This is helpful for stack traces.

```javascript
// bad
var log = function(msg) {
      console.log(msg);
};

// good
var log = function log(msg) {
      console.log(msg);
};
```

_Note: IE8 and below exhibit some quirks with named function expressions.  See [http://kangax.github.io/nfe/](http://kangax.github.io/nfe/) for more info._

### Function arguments

**[eslint: n/a](http://eslint.org/)**

Optional function arguments start with `optional` like `optionalAge`, `optionalName`...

Functions that take a variable number of arguments should have the last argument named `varArgs`. You may not refer to `varArgs` in the code; use the `arguments` array.


### File name

**[eslint: n/a](http://eslint.org/)**

If your file exports a single class, your filename should be exactly the name of the class.

```javascript
// file contents
class CheckBox {
    // ...
}
module.exports = CheckBox;

// in some other file
// bad
var CheckBox = require('./checkBox');

// bad
var CheckBox = require('./check_box');

// good
var CheckBox = require('./CheckBox');
```

**[back to top](#markdown-header-table-of-contents)**

***

## Getters and setters

**[eslint: n/a](http://eslint.org/)**

Getters and setters methods for properties are not required. However, if they are used, then getters must be named `getFoo()` and setters must be named `setFoo(value)`. (For boolean getters, `isFoo()` is also acceptable, and often sounds more natural.)

If you end up with a getter that return the value and a setter that directly set the value, ask your self your those getters and setters are really needed.

```javascript
// bad
dragon.age();

// bad
dragon.age(dragonType);

// good
dragon.getAge();

// bad
dragon.age(25);

// bad
dragon.setAge(25,dragonType);

// good
dragon.setAge(25);

// bad
this.setAge = function(age){
    this.age = age;
};

// good
this.setAge = function(value){
    this.age = value;
};

```

If the property is a boolean, use isVal() or hasVal().

```javascript
// bad
if (!dragon.age()) {
    return false;
}

// good
if (!dragon.hasAge()) {
    return false;
}
```

It's okay to create get() and set() functions, but be consistent.

```javascript
function Jedi(options) {
    options || (options = {});
    var lightsaber = options.lightsaber || 'blue';
    this.set('lightsaber', lightsaber);
}

Jedi.prototype.set = function(key, val) {
    this[key] = val;
};

Jedi.prototype.get = function(key) {
    return this[key];
};
```

**[back to top](#markdown-header-table-of-contents)**

***

## Classes

### Constructor

**[eslint: n/a](http://eslint.org/)**

Assign methods to the prototype object, instead of overwriting the prototype with a new object. Overwriting the prototype makes inheritance impossible: by resetting the prototype you'll overwrite the base!

```javascript
function Jedi() {
    console.log('new jedi');
}

// bad
Jedi.prototype = {
      fight: function fight() {
          console.log('fighting');
      },

      block: function block() {
          console.log('blocking');
      }
};

// good
Jedi.prototype.fight = function fight() {
    console.log('fighting');
};

Jedi.prototype.block = function block() {
    console.log('blocking');
};
```

### Chaining methods with this

**[eslint: n/a](http://eslint.org/)**

Methods **can** return `this` to help with method chaining (useful with the [builder pattern](https://en.wikipedia.org/?title=Builder_pattern)).

```javascript
// bad
Jedi.prototype.jump = function() {
    this.jumping = true;
    return true;
};

Jedi.prototype.setHeight = function(height) {
    this.height = height;
};

var luke = new Jedi();
luke.jump(); // => true
luke.setHeight(20); // => undefined

// good
Jedi.prototype.jump = function() {
    this.jumping = true;
    return this;
};

Jedi.prototype.setHeight = function(height) {
    this.height = height;
    return this;
};

var luke = new Jedi();

luke.jump()
    .setHeight(20);
```

### toString()

**[eslint: n/a](http://eslint.org/)**

It's okay to write a custom toString() method, just make sure it **always** works successfully and causes **no side effects**.

```javascript
function Jedi(options) {
    options || (options = {});
    this.name = options.name || 'no name';
}

Jedi.prototype.getName = function getName() {
    return this.name;
};

Jedi.prototype.toString = function toString() {
    return 'Jedi - ' + this.getName();
};
```

### Modifying prototypes of builtin objects

**[eslint: n/a](http://eslint.org/)**

**Evil!** Modifying builtins like `Object.prototype` and `Array.prototype` are strictly **forbidden**. Modifying other builtins like Function.prototype is less dangerous but still leads to hard to debug issues in production and should be avoided.

### this

**[eslint: n/a](http://eslint.org/)**

Only in object constructors, methods, and in setting up closures. The semantics of `this` can be tricky. At times it refers to the global object (in most places), the scope of the caller (in `eval`), a node in the DOM tree (when attached using an event handler HTML attribute), a newly created object (in a constructor), or some other object (if function was call()ed or apply()ed).

Because this is so easy to get wrong, limit its use to those places where it is **required**:

- in constructors
- in methods of objects (including in the creation of closures)

### Multi-level prototype hierarchies

**[eslint: n/a](http://eslint.org/)**

Not preferred. Multi-level prototype hierarchies are how JavaScript implements inheritance. You have a multi-level hierarchy if you have a user-defined class `D` with another user-defined class `B` as its prototype. These hierarchies are much harder to get right than they first appear!

**[back to top](#markdown-header-table-of-contents)**

***

## Namespaces

**[eslint: n/a](http://eslint.org/)**

You should write your namespaces like this : `com.videotron.xxx`.

JavaScript has no inherent packaging or namespacing support. Global name conflicts are difficult to debug, and can cause intractable problems when two projects try to integrate. In order to make it possible to share common JavaScript code, we've adopted conventions to prevent collisions.

```javascript
var com = {} || com;
com.videotron = {} || com.videotron;
com.videotron.foo = {} || com.videotron.foo;
com.videotron.bar = {} || com.videotron.bar;

com.videotron.foo.Foo = function Foo(){
    this.helloWorld = 'Hello World!';
};

com.videotron.bar.Foo = function Foo(){
    this.helloWorld = '!dlroW olleH';
};
```

**[back to top](#markdown-header-table-of-contents)**

***

## delete

**[eslint: n/a](http://eslint.org/)**

Prefer `this.foo = null;`.

```javascript
Foo.prototype.dispose = function() {
this.property_ = null;
};
```

Instead of:

```javascript
Foo.prototype.dispose = function() {
    delete this.property_;
};
```

**[back to top](#markdown-header-table-of-contents)**

***

## eval()

**[eslint: no-eval](http://eslint.org/docs/rules/no-eval.html)**

Only for code loaders and REPL (Read–eval–print loop). `eval()` makes for confusing semantics and is dangerous to use if the string being eval()'d contains user input. There's usually a better, clearer, and safer way to write your code, so its use is generally not permitted.

For RPC you can always use JSON and read the result using `JSON.parse()` instead of `eval()`.

Let's assume we have a server that returns something like this:

```json
{
    "name": "Alice",
    "id": 31502,
    "email": "looking_glass@example.com"
}
```

```javascript
var userInfo = eval(feed);
var email = userInfo['email'];
```

If the feed was modified to include malicious JavaScript code, then if we use eval then that code will be executed.

```javascript
var userInfo = JSON.parse(feed);
var email = userInfo['email'];
```

With `JSON.parse`, invalid JSON (including all executable JavaScript) will cause an exception to be thrown.

**[back to top](#markdown-header-table-of-contents)**

***

## with(){}

**[eslint: no-with](http://eslint.org/docs/rules/no-with.html)**

**Evil!** Using `with` clouds the semantics of your program. Because the object of the with can have properties that collide with local variables, it can drastically change the meaning of your program. For example, what does this do?

```javascript
with (foo) {
    var x = 3;
    return x;
}
```

Answer: anything. The local variable `x` could be clobbered by a property of `foo` and perhaps it even has a setter, in which case assigning `3` could cause lots of other code to execute. **Don't use** `with`.

**[back to top](#markdown-header-table-of-contents)**

***

## console.log()

**[eslint: no-console](http://eslint.org/docs/rules/no-console.html)**

You should never commit a `console.log()` statement.

Logging on the client side is useless unless if the logs are sent to a server.

**[back to top](#markdown-header-table-of-contents)**

***

## Events

**[eslint: n/a](http://eslint.org/)**

When attaching data payloads to events (whether DOM events or something more proprietary like Backbone events), pass a hash instead of a raw value. This allows a subsequent contributor to add more data to the event payload without finding and updating every handler for the event. For example, instead of:

```javascript
// bad
$(this).trigger('listingUpdated', listing.id);
...

$(this).on('listingUpdated', function(e, listingId) {
  // do something with listingId
});
```

prefer:

```javascript
// good
$(this).trigger('listingUpdated', { listingId : listing.id });

...

$(this).on('listingUpdated', function(e, data) {
  // do something with data.listingId
});
```

**[back to top](#markdown-header-table-of-contents)**

***

## Exceptions

**[eslint: no-throw-literal](http://eslint.org/docs/rules/no-throw-literal.html)**

**Yes!** You basically can't avoid exceptions if you're doing something non-trivial (using an application development framework, etc.). Go for it.

```javascript
// bad
throw 'Oops!';

// good
throw new Error('Oops!');
```

### Custom exceptions

Yes! Without custom exceptions, returning error information from a function that also returns a value can be tricky, not to mention inelegant. Bad solutions include passing in a reference type to hold error information or always returning Objects with a potential error member. These basically amount to a primitive exception handling hack. Feel free to use custom exceptions when appropriate.

**[back to top](#markdown-header-table-of-contents)**

***

## Code formatting

We follow the [C++ formatting rules](https://google-styleguide.googlecode.com/svn/trunk/cppguide.xml#Formatting) in spirit, with the following additional clarifications.

### Curly Braces

Because of implicit semicolon insertion, always start your curly braces on the same line as whatever they're opening. For example:

```javascript
if (something) {
// ...
} else {
// ...
}
```

### Array and Object Initializers

Single-line array and object initializers are allowed when they fit on a line:

```javascript
var arr = [1, 2, 3];  // No space after [ or before ].
var obj = {a: 1, b: 2, c: 3};  // No space after { or before }.
```

Multiline array initializers and object initializers are indented 2 spaces, with the braces on their own line, just like blocks.

```javascript
// Object initializer.
var inset = {
top: 10,
right: 20,
bottom: 15,
left: 12
};

// Array initializer.
this.rows_ = [
    '"Slartibartfast" <fjordmaster@magrathea.com>',
    '"Zaphod Beeblebrox" <theprez@universe.gov>',
    '"Ford Prefect" <ford@theguide.com>',
    '"Arthur Dent" <has.no.tea@gmail.com>',
    '"Marvin the Paranoid Android" <marv@googlemail.com>',
    'the.mice@magrathea.com'
];

// Used in a method call.
goog.dom.createDom(goog.dom.TagName.DIV, {
    id: 'foo',
    className: 'some-css-class',
    style: 'display:none'
}, 'Hello, world!');
```

Long identifiers or values present problems for aligned initialization lists, so always prefer non-aligned initialization. For example:

```javascript
CORRECT_Object.prototype = {
    a: 0,
    b: 1,
    lengthyName: 2
};
```
Not like this:

```javascript
WRONG_Object.prototype = {
    a          : 0,
    b          : 1,
    lengthyName: 2
};
```

### Function Arguments

When possible, all function arguments should be listed on the same line. If doing so would exceed the 80-column limit, the arguments must be line-wrapped in a readable way. To save space, you may wrap as close to 80 as possible, or put each argument on its own line to enhance readability. The indentation may be either four spaces, or aligned to the parenthesis. Below are the most common patterns for argument wrapping:

```javascript
// Four-space, wrap at 80.  Works with very long function names, survives
// renaming without reindenting, low on space.
goog.foo.bar.doThingThatIsVeryDifficultToExplain = function(
    veryDescriptiveArgumentNumberOne, veryDescriptiveArgumentTwo,
    tableModelEventHandlerProxy, artichokeDescriptorAdapterIterator) {
// ...
};

// Four-space, one argument per line.  Works with long function names,
// survives renaming, and emphasizes each argument.
goog.foo.bar.doThingThatIsVeryDifficultToExplain = function(
    veryDescriptiveArgumentNumberOne,
    veryDescriptiveArgumentTwo,
    tableModelEventHandlerProxy,
    artichokeDescriptorAdapterIterator) {
// ...
};

// Parenthesis-aligned indentation, wrap at 80.  Visually groups arguments,
// low on space.
function foo(veryDescriptiveArgumentNumberOne, veryDescriptiveArgumentTwo,
      tableModelEventHandlerProxy, artichokeDescriptorAdapterIterator) {
// ...
}

// Parenthesis-aligned, one argument per line.  Emphasizes each
// individual argument.
function bar(veryDescriptiveArgumentNumberOne,
    veryDescriptiveArgumentTwo,
    tableModelEventHandlerProxy,
    artichokeDescriptorAdapterIterator) {
// ...
}
```

### Passing Anonymous Functions

When declaring an anonymous function in the list of arguments for a function call, the body of the function is indented two spaces from the left edge of the statement, or two spaces from the left edge of the function keyword. This is to make the body of the anonymous function easier to read (i.e. not be all squished up into the right half of the screen).

```javascript
prefix.something.reallyLongFunctionName('whatever', function(a1, a2) {
    if (a1.equals(a2)) {
        someOtherLongFunctionName(a1);
    } else {
        andNowForSomethingCompletelyDifferent(a2.parrot);
    }
});

var names = prefix.something.myExcellentMapFunction(
    verboselyNamedCollectionOfItems,
    function(item) {
        return item.name;
    });
```

### Indenting wrapped lines

Except for array literals, object literals, and anonymous functions, all wrapped lines should be indented either left-aligned to a sibling expression above, or four spaces (not two spaces) deeper than a parent expression (where "sibling" and "parent" refer to parenthesis nesting level).

```javascript
someWonderfulHtml = '' +
                    getEvenMoreHtml(someReallyInterestingValues, moreValues,
                                    evenMoreParams, 'a duck', true, 72,
                                    slightlyMoreMonkeys(0xfff)) +
                    '';

thisIsAVeryLongVariableName =
    hereIsAnEvenLongerOtherFunctionNameThatWillNotFitOnPrevLine();

thisIsAVeryLongVariableName = siblingOne + siblingTwo + siblingThree +
    siblingFour + siblingFive + siblingSix + siblingSeven +
    moreSiblingExpressions + allAtTheSameIndentationLevel;

thisIsAVeryLongVariableName = operandOne + operandTwo + operandThree +
    operandFour + operandFive * (
        aNestedChildExpression + shouldBeIndentedMore);

someValue = this.foo(
    shortArg,
    'Some really long string arg - this is a pretty common case, actually.',
    shorty2,
    this.bar());

if (searchableCollection(allYourStuff).contains(theStuffYouWant) &&
    !ambientNotification.isActive() && (client.isAmbientSupported() ||
                                        client.alwaysTryAmbientAnyways())) {
    ambientNotification.activate();
}
```

### Blank lines

Use newlines to group logically related pieces of code. For example:

```javascript
doSomethingTo(x);
doSomethingElseTo(x);
andThen(x);

nowDoSomethingWith(y);

andNowWith(z);
```

### Binary and Ternary Operators

Always put the operator on the preceding line. Otherwise, line breaks and indentation follow the same rules as in other Google style guides. This operator placement was initially agreed upon out of concerns about automatic semicolon insertion. In fact, semicolon insertion cannot happen before a binary operator, but new code should stick to this style for consistency.

```javascript
var x = a ? b : c;  // All on one line if it will fit.

// Indentation +4 is OK.
var y = a ?
    longButSimpleOperandB : longButSimpleOperandC;

// Indenting to the line position of the first operand is also OK.
var z = a ?
    moreComplicatedB :
    moreComplicatedC;
This includes the dot operator.

var x = foo.bar().
  doSomething().
  doSomethingElse();
```

This includes the dot operator.

```
#!javascript
  var x = foo.bar().
      doSomething().
      doSomethingElse();
```

**[back to top](#markdown-header-table-of-contents)**

***

## jQuery

Prefix jQuery object variables with a `$`.

```javascript
// bad
var sidebar = $('.sidebar');

// good
var $sidebar = $('.sidebar');
```

Cache jQuery lookups.

```javascript
// bad
function setSidebar() {
  $('.sidebar').hide();

  // ...stuff...

    $('.sidebar').css({
        'background-color': 'pink'
    });
}

// good
function setSidebar() {
  var $sidebar = $('.sidebar');
  $sidebar.hide();

  // ...stuff...

    $sidebar.css({
        'background-color': 'pink'
    });
}
```

For DOM queries use Cascading `$('.sidebar ul')` or parent > child `$('.sidebar > ul')`. [jsPerf](http://jsperf.com/jquery-find-vs-context-sel/16)

Use `find` with scoped jQuery object queries.

```javascript
// bad
$('ul', '.sidebar').hide();

// bad
$('.sidebar').find('ul').hide();

// good
$('.sidebar ul').hide();

// good
$('.sidebar > ul').hide();

// good
$sidebar.find('ul').hide();
```

**[back to top](#markdown-header-table-of-contents)**

***

## Standards features

Always preferred over non-standards features. For maximum portability and compatibility, always prefer standards features over non-standards features (e.g., `string.charAt(3)` over `string[3]` and element access with DOM functions instead of using an application-specific shorthand).

**[back to top](#markdown-header-table-of-contents)**

***

## ECMAScript 5 Compatibility

Refer to [Kangax](https://twitter.com/kangax/)'s ES5 [compatibility table](http://kangax.github.com/es5-compat-table/).

**[back to top](#markdown-header-table-of-contents)**

***

## Internet Explorer's Conditional Comments

Don't do this:

```javascript
var f = function () {
    /*@cc_on if (@_jscript) { return 2* @*/  3; /*@ } @*/
};
```

Conditional Comments hinder automated tools as they can vary the JavaScript syntax tree at runtime.

**[back to top](#markdown-header-table-of-contents)**

***

## Testing

```javascript
function() {
    return true;
}
```

**[back to top](#markdown-header-table-of-contents)**

***

## Examples

```
#!javascript
(function($){

  var MAIN_COMPONENT_SELECTOR = '.main'

  function MainComponent(element){
    var CTA_BUTTON_COMPONENT_SELECTOR = '.cta-button';

    var ctaButtonComponent;

    function initialize(){
        var ctaButtonElement = $(CTA_BUTTON_COMPONENT_SELECTOR, element);
        ctaButtonComponent = new CTAButtonComponent(ctaButtonElement);
    }

    this.reset = function(){
        ctaButtonComponent.reset();
    };

    this.destroy = function(){
        ctaButtonComponent.destroy();
        ctaButtonComponent = null;
    };

    initialize();
    
  }

  function CTAButtonComponent(element){

    var DEFAULT_MARGIN_LEFT = '10px';
    var RANDOM_MARGIN_RANGE = 100;

    function initialize(){
        if(!element || element.length !== 1){
            throw new Error('Oops');
        }
        element.on('click',elementClick);
    }

    function  generateRandomMarginLeftValue(){
        return Math.floor(Math.random() * RANDOM_MARGIN_RANGE) + 'px';
    }

    function setElementMarginLeft(value){
        element.css('margin-left',value);
    }

    function elementClick(){
        setElementMarginLeft(generateRandomMarginLeftValue());
    }

    this.reset = function(){
        setElementMarginLeft(DEFAULT_MARGIN_LEFT);
    };

    this.destroy = function(){
        element.off('click',elementClick);
        element = null;
    };

    initialize();

  }

    function initialize(){
        var mainComponent = new MainComponent($(MAIN_COMPONENT_SELECTOR));
        mainComponent.reset();

  }

  $(initialize);

})(jQuery);
```

**[back to top](#markdown-header-table-of-contents)**

***

##Credits

- [Google javascript style guide](https://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml)
- [airbnb ES5 style guide](https://github.com/airbnb/javascript/blob/master/es5/README.md#license)

**[back to top](#markdown-header-table-of-contents)**
